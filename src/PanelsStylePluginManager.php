<?php

namespace Drupal\panels_style;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\YamlDiscoveryDecorator;

/**
 * Plugin type manager for all styles.
 */
class PanelsStylePluginManager extends DefaultPluginManager implements PanelsStylePluginManagerInterface {

  /**
   * Constructs a PanelsStylePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct("Plugin/PanelsStyle", $namespaces, $module_handler, 'Drupal\panels_style\Plugin\PanelsStyle\PanelsStyleInterface', 'Drupal\panels_style\Annotation\PanelsStyle');
    $this->discovery = new YamlDiscoveryDecorator($this->getDiscovery(), 'styles', $module_handler->getModuleDirectories());

    $this->setCacheBackend($cache_backend, 'panels_style_plugins');
    $this->alterInfo('panels_style_plugin');
  }

  /**
   * {inheritdoc}
   */
  public function getPluginOptions($type = null) {
    $options = [];
    foreach ($this->getDefinitions() as $definition) {
      if (!$type || !empty($definition[$type])) {
        $options[$definition['id']] = $definition['title'];
      }
    }
    return $options;
  }

  /**
   * Get a style plugin instance.
   */
  public function createInstanceFromConfiguration($style_config, $style_plugin_id = '') {
    $default = $this->configFactory->get('panels.settings')->get('default_style_' . $type);

    $plugin_id = !empty($style_config['plugin_id']) ? $style_config['plugin_id'] : $default;
    $plugin_configuration = !empty($style_config['configuration']) ? $style_config['configuration'] : [];

    if (!empty($style_plugin_id) && $style_plugin_id !== $plugin_id) {
      $plugin_id = $style_plugin_id;
      $plugin_configuration = [];
    }

    return $this->styleManager->createInstance($plugin_id, $plugin_configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $definitions = parent::getDefinitions();
    // Don't return the fallback plugin definition
    unset($definitions['panels_broken']);
    return $definitions;
  }

  /**
   * {inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'panels_broken';
  }

}
