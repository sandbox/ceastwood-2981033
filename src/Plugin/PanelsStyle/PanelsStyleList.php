<?php

namespace Drupal\panels_style\Plugin\PanelsStyle;

use Drupal\panels\Plugin\DisplayVariant\PanelsDisplayVariant;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the list panels style plugin.
 *
 * @PanelsStyle(
 *   id = "panels_list",
 *   title = @Translation("List"),
 *   block = FALSE
 * )
 */
class PanelsStyleList extends PanelsStyleDefault {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'type' => 'ul',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function processRegion(array &$build, $region_id, PanelsDisplayVariant $panels_display) {
    $config = $this->getConfiguration();
    $blocks = $this->getRegionBlocks($build);
    $build = [
      '#theme' => 'item_list',
      '#list_type' => $config['type'],
      '#items' => $blocks,
    ];
    if ($config['classes']) {
      $build['#attributes']['classes'] = explode(' ', $config['classes']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $config = $this->getConfiguration();

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => [
        'ul' => $this->t('Unordered'),
        'ol' => $this->t('Ordered'),
      ],
      '#default_value' => $config['type'],
    ];

    // Put the classes option after the type
    $form = parent::buildConfigurationForm($form, $form_state);

    return $form;
  }

}
