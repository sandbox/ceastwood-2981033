<?php

namespace Drupal\panels_style\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\panels_style\PanelsStylePluginManager;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Drupal\panels_style\PanelsStyleHelper definition.
   *
   * @var \Drupal\panels_style\PanelsStyleHelper
   */
  protected $panelsStyleManager;
  /**
   * Constructs a new SettingsForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PanelsStylePluginManager $panels_style_manager) {
    parent::__construct($config_factory);
    $this->panelsStyleManager = $panels_style_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.panels_style.style')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'panels_style.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'panels_style_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('panels_style.settings');

    $form['default_style_region'] = [
      '#type' => 'select',
      '#title' => $this->t('Default Region Style'),
      '#description' => $this->t('The default style plugin to use for regions. Existing regions that existed before the panels style module was enabled will also use this default.'),
      '#options' => $this->panelsStyleManager->getPluginOptions('region'),
      '#default_value' => $config->get('default_style_region'),
    ];

    $form['default_style_block'] = [
      '#type' => 'select',
      '#title' => $this->t('Default Block Style'),
      '#description' => $this->t('The default style plugin to use for blocks. Existing blocks that existed before the panels style module was enabled will also use this default.'),
      '#options' => $this->panelsStyleManager->getPluginOptions('block'),
      '#default_value' => $config->get('default_style_block'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('panels_style.settings')
      ->set('default_style_region', $form_state->getValue('default_style_region'))
      ->set('default_style_block', $form_state->getValue('default_style_block'))
      ->save();
  }

}
