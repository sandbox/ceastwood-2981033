<?php

namespace Drupal\panels_style\Plugin\PanelsStyle;

use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\panels\Plugin\DisplayVariant\PanelsDisplayVariant;

/**
 * Provides an interface for PanelsStyle plugins.
 */
interface PanelsStyleInterface extends ConfigurablePluginInterface, PluginInspectionInterface, PluginFormInterface {

  /**
   * Process a panels region.
   *
   * @param array $build
   *   The build array for the region to be modified.
   * @param string $region_id
   *   The region id.
   * @param PanelsDisplayVariant $panels_display
   *   The Panels display variant.
   */
  public function processRegion(array &$build, $region_id, PanelsDisplayVariant $panels_display);

  /**
   * Process a panels block.
   *
   * @param array $build
   *   The build array for the block to be modified.
   * @param BlockPluginInterface $block_id
   *   The block plugin object.
   * @param PanelsDisplayVariant $panels_display
   *   The Panels display variant.
   */
  public function processBlock(array &$build, BlockPluginInterface $block, PanelsDisplayVariant $panels_display);

}
