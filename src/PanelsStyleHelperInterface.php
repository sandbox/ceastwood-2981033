<?php

namespace Drupal\panels_style;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\panels_style\PanelsStyleHelperInterface;
use Drupal\panels_style\PanelsStylePluginManagerInterface;
use Drupal\ctools\Plugin\BlockVariantInterface;

/**
 * Panels styles helper service.
 */
interface PanelsStyleHelperInterface {

  /**
   * Returns the panels style plugin for a block.
   *
   * @param array $block_style_config
   *   The display variant's block style configuration. This is what's saved
   *   in the display config. It's an array with two keys:
   *   - plugin_id: The panels style plugin id
   *   - plugin_configuration: The configuration for the style plugin
   *
   * @return \Drupal\panels\Plugin\PanelsStyle\PanelsStyleInterface
   *   The style plugin.
   */
  public function getBlockStyle($block_style_config);

  /**
   * Returns the panels style plugin for a region.
   *
   * @param string $region_style_config
   *   The display variant's region style configuration. This is what's saved
   *   in the display config. It's an array with two keys:
   *   - plugin_id: The panels style plugin id
   *   - plugin_configuration: The configuration for the style plugin
   *
   * @return \Drupal\panels\Plugin\PanelsStyle\PanelsStyleInterface
   *   The style plugin.
   */
  public function getRegionStyle($region_style_config);

  /**
   * Get the variant plugin from the tempstore cache.
   *
   * @param  string $machine_name
   *   The variant machine name
   * @param  string $tempstore_id
   *   The tempstore id of the cached values
   * @return \Drupal\panels\Plugin\DisplayVariant\PanelsDisplayVariant
   *   The variant plugin object with the cached values applied.
   */
  public function getTempstoreDisplayVariant($machine_name, $tempstore_id);

  /**
   * Update a tempstore cached variant's block config.
   *
   * @param  string $block_id
   * @param  \Drupal\panels\Plugin\PanelsStyle\PanelsStyleInterface $style_plugin
   * @param  string $tempstore_id
   * @param  string $machine_name
   * @return void
   */
  public function updateTempstoreDisplayVariantBlockConfig($block_id, $style_plugin, $tempstore_id, $machine_name);

  /**
   * Update a tempstore cached variant's block config, specifically for the
   * IPE block form.
   *
   * @param  string $block_id
   * @param  \Drupal\panels\Plugin\PanelsStyle\PanelsStyleInterface $style_plugin
   * @param  string $panels_display
   * @return void
   */
  public function updateIPETempstoreDisplayVariantBlockConfig($block_id, $style_plugin, $panels_display);

}
