<?php

namespace Drupal\panels_style\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
// use Drupal\Core\Plugin\Context\Context;
// use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\ContextAwarePluginAssignmentTrait;
// use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\panels\CachedValuesGetterTrait;
use Drupal\user\SharedTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\panels_style\PanelsStyleHelperInterface;


/**
 * Provides a form for customizing a region's styles.
 */
class PanelsStyleRegionEditStyleForm extends FormBase {

  use ContextAwarePluginAssignmentTrait;
  use CachedValuesGetterTrait;

  /**
   * Tempstore factory.
   *
   * @var \Drupal\user\SharedTempStoreFactory
   */
  protected $tempstore;

  /**
   * The tempstore id.
   *
   * @var string
   */
  protected $tempstore_id;

  /**
   * The variant plugin.
   *
   * @var \Drupal\panels\Plugin\DisplayVariant\PanelsDisplayVariant
   */
  protected $variantPlugin;

  /**
   * The style plugin being used.
   *
   * @var \Drupal\panels_style\PanelsStyleInterface
   */
  protected $stylesHelper;

  /**
   * The style plugin being used.
   *
   * @var \Drupal\panels_style\Plugin\PanelsStyle\PanelsStyleInterface
   */
  protected $stylePlugin;

  /**
   * Constructs a new VariantPluginConfigureBlockFormBase.
   *
   * @param \Drupal\user\SharedTempStoreFactory $tempstore
   *   The tempstore factory.
   */
  public function __construct(SharedTempStoreFactory $tempstore, PanelsStyleHelperInterface $styles_helper) {
    $this->tempstore = $tempstore;
    $this->stylesHelper = $styles_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.shared_tempstore'),
      $container->get('panels_style.helper')
    );
  }

  /**
   * Get the tempstore id.
   *
   * @return string
   */
  protected function getTempstoreId() {
    return $this->tempstore_id;
  }

  /**
   * Get the tempstore.
   *
   * @return \Drupal\user\SharedTempStore
   */
  protected function getTempstore() {
    return $this->tempstore->get($this->getTempstoreId());
  }

  /**
   * Gets the variant plugin for this page variant entity.
   *
   * @return \Drupal\panels\Plugin\DisplayVariant\PanelsDisplayVariant
   */
  protected function getVariantPlugin() {
    return $this->variantPlugin;
  }

  /**
   * {@inheritdoc}
   */
  protected function submitText() {
    return $this->t('Update style');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'panels_region_edit_style_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $tempstore_id = NULL, $machine_name = NULL, $region = NULL) {
    $this->tempstore_id = $tempstore_id;
    $cached_values = $this->getCachedValues($this->tempstore, $tempstore_id, $machine_name);
    $this->variantPlugin = $cached_values['plugin'];
    $contexts = $this->variantPlugin->getPattern()->getDefaultContexts($this->tempstore, $this->getTempstoreId(), $machine_name);
    $this->variantPlugin->setContexts($contexts);
    $form_state->setTemporaryValue('gathered_contexts', $contexts);

    $display_configuration = $this->variantPlugin->getConfiguration();
    if ($form_state->getValue('style')) {
      $region_style_config = ['plugin_id' => $form_state->getValue('style')];
    }
    else {
      $region_style_config = !empty($display_configuration['region_styles'][$region]) ? $display_configuration['region_styles'][$region] : [];
    }

    $this->stylePlugin = $this->stylesHelper->getRegionStyle($region_style_config, $form_state->getValue('style'));

    $form_state->set('region', $region);
    $form_state->set('machine_name', $machine_name);

    $form['#tree'] = TRUE;

    $plugin_options = \Drupal::service('plugin.manager.panels_style.style')->getPluginOptions('region');

    $form['style'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Style Plugin'),
      '#description'   => $this->t('Please select the style plugin for this region.'),
      '#options'       => $plugin_options,
      '#default_value' => $this->stylePlugin->getPluginId(),
      '#ajax' => array(
        'callback' => '::updateStyleSettings',
        'wrapper' => 'edit-style-settings',
        'progress' => array(
          'type' => 'throbber',
          'message' => $this->t('Updating style settings...'),
        ),
      ),
    ];

    $form['style_settings'] = $this->stylePlugin->buildConfigurationForm([], (new FormState())->setValues($form_state->getValue('style_settings', [])));
    $form['style_settings']['#prefix'] = '<div id="edit-style-settings">';
    $form['style_settings']['#suffix'] = '</div>';
    $form['style_settings']['#tree'] = TRUE;

    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->submitText(),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * Handles AJAX callbacks upon changes of style setting.
   *
   * @param array $form
   *   An associative array containing the structure of the plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure of those parts of the form to replace style settings.
   */
  public function updateStyleSettings(array &$form, FormStateInterface $form_state) {
    return $form['style_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $style_settings = (new FormState())->setValues($form_state->getValue('style_settings', []));
    $this->stylePlugin->validateConfigurationForm($form['style_settings'], $style_settings);
    $form_state->setValue('style_settings', $style_settings->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Pull the styles config
    $style_settings = (new FormState())->setValues($form_state->getValue('style_settings', []));
    $this->stylePlugin->submitConfigurationForm($form['style_settings'], $style_settings);
    $style_configuration = $this->stylePlugin->getConfiguration();

    // Apply the styles configuration to the display variant config
    $region_style_config = [
      'plugin_id' => $this->stylePlugin->getPluginId(),
      'configuration' => $style_configuration,
    ];
    $region = $form_state->get('region');
    $display_configuration = $this->variantPlugin->getConfiguration();
    $display_configuration['region_styles'][$region] = $region_style_config;
    $this->variantPlugin->setConfiguration($display_configuration);
    $form_state->setValue('style_settings', $style_settings);

    // Save the display variant
    $cached_values = $this->getCachedValues($this->tempstore, $this->tempstore_id, $form_state->get('machine_name'));
    $cached_values['plugin'] = $this->getVariantPlugin();
    // PageManager specific handling.
    if (isset($cached_values['page_variant'])) {
      $cached_values['page_variant']->getVariantPlugin()->setConfiguration($cached_values['plugin']->getConfiguration());
    }
    $this->getTempstore()->set($cached_values['id'], $cached_values);
  }
}
